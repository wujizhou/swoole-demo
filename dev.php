<?php
return [
    'SERVER_NAME' => "EasySwoole-demo",
    'MAIN_SERVER' => [
        'LISTEN_ADDRESS' => '0.0.0.0',
        'PORT' => 9501,
        'SERVER_TYPE' => EASYSWOOLE_WEB_SERVER, //可选为 EASYSWOOLE_SERVER  EASYSWOOLE_WEB_SERVER EASYSWOOLE_WEB_SOCKET_SERVER,EASYSWOOLE_REDIS_SERVER
        'SOCK_TYPE' => SWOOLE_TCP,
        'RUN_MODEL' => SWOOLE_PROCESS,
        'SETTING' => [
            'worker_num' => 8,
            'reload_async' => true,
            'max_wait_time'=>3
        ],
        'TASK'=>[
            'workerNum'=>4,
            'maxRunningNum'=>128,
            'timeout'=>15
        ]
    ],
    'TEMP_DIR' => null,
    'LOG_DIR' => null,
    'DISPLAY_ERROR' => true,//是否开启错误显示
    'MYSQL' =>[
        'host'          => '127.0.0.1',
        'port'          => 3306,
        'user'          => 'swoole',
        'password'      => 'test_2613747851',
        'database'      => 'swoole_demo',
        'timeout'       => 3,
        'connect_timeout'=>5,
        'charset'       => 'utf8mb4',
    ]
];
