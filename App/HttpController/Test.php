<?php
/**
 * Author      : rainbow <977746075@qq.com>
 * DateTime    : 2019-11-08 10:38:43
 * Description : [Description]
 */
namespace App\HttpController;
use src\base\BaseController;
use EasySwoole\Mysqli\Mysqli;
use EasySwoole\Mysqli\Config;

class Test extends BaseController {
  function index(){
    $this->response()->write(test(rand(100,400)) . "-index4");
  }
  function mysql(){
      $configArr = \EasySwoole\EasySwoole\Config::getInstance()->getConf("MYSQL");
      $config = new Config($configArr);
      $db = new Mysqli($config);
      var_dump($db);
  }
}
