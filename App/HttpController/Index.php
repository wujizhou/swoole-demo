<?php
namespace App\HttpController;
use src\base\BaseController;
class Index extends BaseController {
    function index() {
        $file = EASYSWOOLE_ROOT.'/vendor/easyswoole/easyswoole/src/Resource/Http/welcome.html';
        if(!is_file($file)){
            $file = EASYSWOOLE_ROOT.'/src/Resource/Http/welcome.html';
        }
        $this->response()->write("1".file_get_contents($file));
    }
}